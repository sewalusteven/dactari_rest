const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
require("dotenv").config()

global.__basedir = __dirname;
//initialize application

const app = express();

//middleware
app.use(bodyParser.json());
app.use(cors());

//api routes
const patients = require('./routes/api/patients/patients');
const doctors  = require('./routes/api/doctors/doctors');
const users = require('./routes/api/users');
const docRequests = require('./routes/api/doctors/requests');
const pharmRequests = require('./routes/api/pharmacies/requests');
const pharmacies = require('./routes/api/pharmacies/pharmacies');
const specialities = require('./routes/api/specialities')
const auth = require('./routes/api/auth');
const drugs = require('./routes/api/drugs')


app.use('/api/patients', patients);
app.use('/api/doctors',doctors);
app.use('/api/users', users);
app.use('/api/doctor-requests',docRequests);
app.use('/api/pharmacy-requests',pharmRequests);
app.use('/api/pharmacies', pharmacies);
app.use('/api/specialities', specialities);
app.use('/api/auth', auth);
app.use('/api/drugs',drugs);

//port
const port = process.env.PORT || 5000;

//start server
app.listen(port,()=> console.log(`Server started on port ${port}`));
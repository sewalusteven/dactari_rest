const express = require('express');
const mongodb = require('mongodb');
const bcrypt = require('bcrypt');
const router = express.Router();
const helper = require('../../helpers/misc')

// Connection URL
const url = 'mongodb://mongo:27017';

// Database Name
const dbName = 'dactari';


//Get Users
router.post('/', async (req, res) => {
    const users = await loadUsersCollection();
    const current_page = req.body.page || 1;
    const per_page = req.body.limit || process.env.PER_PAGE;

    res.status(201).send(helper.paginator(await users.find({}).toArray(),current_page, per_page));
})

router.get('/single/:id', async (req, res) => {
    const users = await loadUsersCollection();
    res.send(await users.find({_id: new mongodb.ObjectID(req.params.id)}).toArray());

})


//Add Users
router.post('/add', async (req, res) => {
    const users = await loadUsersCollection();

    bcrypt.genSalt(10, function(err, salt) {
        if(err) throw err;
        bcrypt.hash(req.body.password, salt, function(err, hash) {
            if(err) throw err;
            // Store hash in your password DB.
            users.insertOne({
                fullName: req.body.fullName,
                email: req.body.email,
                phoneNumber: req.body.phoneNumber,
                picture: null,
                password: hash,
                createdAt: new Date()
            }).then(results => {
                res.status(201).json(results);
            }).catch(err => {
                console.log(err)
            })
        });
    });



})

//Update Users
router.put('/update/:id', async (req, res) => {
    const users = await loadUsersCollection();
    await users.findOneAndUpdate({ _id: new mongodb.ObjectID(req.params.id) }, {
        $set: req.body
    })

    res.status(201).send({ msg: "User Updated Successfully"})
})

router.put('/password/:id', async (req, res) => {
    const users = await loadUsersCollection();

    bcrypt.genSalt(10, function(err, salt) {
        if(err) throw err;
        bcrypt.hash(req.body.password, salt, function(err, hash) {
            if(err) throw err;
            // Store hash in your password DB.
            users.findOneAndUpdate({ _id: new mongodb.ObjectID(req.params.id) }, {
                $set: {
                    password: hash
                }
            }).then(results => {
                res.status(201).send({ msg: "Password Updated Successfully"})
            }).catch(err => {
                console.log(err)
            })
        });
    });




})


//Delete Users


//connection
async function loadUsersCollection() {
    const client = await mongodb.MongoClient.connect(url, { useUnifiedTopology: true });

    return client.db(dbName).collection('users')
}


module.exports = router
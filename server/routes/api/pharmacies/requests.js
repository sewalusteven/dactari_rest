const express = require('express');
const mongodb = require('mongodb');
const router = express.Router();

//helper
const helper = require('../../../helpers/misc')

// Connection URL
const url = 'mongodb://mongo:27017';

// Database Name
const dbName = 'dactari';

//insert request
router.post('/add', async (req, res) => {
    const pharmRequests = await loadPharmacyRequestCollection()
    pharmRequests.insertOne({
        phoneNumber: req.body.phoneNumber,
        requestType: req.body.requestType,
        totalAmount: req.body.totalAmount,
        drugs: req.body.drugs,
        requestDate: req.body.requestDate,
        pharmacy: req.body.pharmacy,
        paymentStatus: false,
        status: "Pending",
        paymentMethod: req.body.paymentMethod,
        patientId: req.body.patientId,
    }).then(results => {
        console.log(results);
        res.status(201).send({ msg: "Request Added Successfully!!!" })
    }).catch(err => {
        console.log(err)
        res.status(401).send({ msg: 'Something went wrong!!!' })
    })
});

//get requests
router.post('/', async (req, res) => {
    const pharmRequests = await loadPharmacyRequestCollection();
    const current_page = req.body.page || 1;
    const per_page = req.body.limit || process.env.PER_PAGE;

    res.status(201).send(helper.paginator(await pharmRequests.find({}).toArray(), current_page, per_page));


})

router.post('/patient/:id', async (req, res) => {
    const pharmRequests = await loadPharmacyRequestCollection();
    const current_page = req.body.page || 1;
    const per_page = req.body.limit || process.env.PER_PAGE;

    res.status(201).send(helper.paginator(await pharmRequests.find({ patientId: req.params.id }).toArray(), current_page, per_page));


})

router.get('/single/:id', async (req, res) => {
    const pharmRequests = await loadPharmacyRequestCollection()
    res.send(await pharmRequests.find({ _id: new mongodb.ObjectID(req.params.id) }).toArray());
})

//update requests
router.put('/update/:id', async (req, res) => {
    const pharmRequests = await loadPharmacyRequestCollection()
    await pharmRequests.findOneAndUpdate({ _id: new mongodb.ObjectID(req.params.id) }, {
        $set: req.body
    })

    res.status(201).send({ msg: "Request Updated Successfully" })
})


async function loadPharmacyRequestCollection() {
    const client = await mongodb.MongoClient.connect(url, { useUnifiedTopology: true })

    return client.db(dbName).collection('pharmacy_requests');

}

module.exports = router;
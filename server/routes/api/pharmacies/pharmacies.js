const express = require('express');
const mongodb = require('mongodb');
const router = express.Router();

//helpers
const helper = require('../../../helpers/misc')

// Connection URL
const url = 'mongodb://mongo:27017';

// Database Name
const dbName = 'dactari';


//Get Pharmacies
router.post('/', async (req, res) => {
    const pharmacies = await loadPharmaciesCollection();

    const current_page = req.body.page || 1;
    const per_page = req.body.limit || process.env.PER_PAGE;

    res.status(201).send(helper.paginator(await pharmacies.find({}).toArray(),current_page, per_page));

})

router.get('/single/:id', async (req, res) => {
    const pharmacies = await loadPharmaciesCollection()
    res.send(await pharmacies.find({_id: new mongodb.ObjectID(req.params.id)}).toArray());

})


//Add Pharmacies
router.post('/add', async (req, res) => {
    const pharmacies = await loadPharmaciesCollection();
    await pharmacies.insertOne({
        name: req.body.name,
        contactPerson: req.body.contactPerson,
        phone: req.body.phone,
        city: req.body.city,
        address: req.body.address,
        createdAt: new Date()
    })

    res.status(201).send({ msg : "Pharmacy added successfully"});
})

//Update Pharmacies
router.put('/update/:id', async (req, res) => {
    const pharmacies = await loadPharmaciesCollection()
    await pharmacies.findOneAndUpdate({ _id: new mongodb.ObjectID(req.params.id) }, {
        $set: req.body
    })

    res.status(201).send({ msg: "Pharmacy Updated Successfully"})
})



//Delete Patients


//connection
async function loadPharmaciesCollection() {
    const client = await mongodb.MongoClient.connect(url, { useUnifiedTopology: true });

    return client.db(dbName).collection('pharmacies')
}


module.exports = router
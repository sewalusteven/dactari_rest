const express = require('express');
const mongodb = require('mongodb');
const router = express.Router();
const helper = require('../../helpers/misc')

//the real deal begins here (upload and convert to json any excel file)
const fs = require('fs');
const multer = require('multer');
const excelToJson = require('convert-excel-to-json');

const upload = multer({ dest: __basedir + '/uploads/' });

router.post('/upload/:id', upload.single("file"), async (req, res) => {
    await importExcelData2MongoDB(__basedir + '/uploads/' + req.file.filename, req.params.id);
    res.status(201).json({
        'msg': 'Drug File uploaded/import successfully!', 'file': req.file
    });
});

router.post('/:id', async (req, res) => {
    const drugs = await loadDrugsCollection();
    const current_page = req.body.page || 1;
    const per_page = req.body.limit || process.env.PER_PAGE;

    res.status(201).send(helper.paginator(await drugs.find({ pharmacyId: req.params.id }).toArray(), current_page, per_page));
})

router.post('/search/:id', async (req, res) => {
    const drugs = await loadDrugsCollection();
    // Create the index
    await drugs.createIndex({ name: "text" })

    const searchResults = await drugs.find({ '$text': { '$search': req.body.search }, pharmacyId: req.params.id }).toArray();
    res.status(201).send(searchResults);


})

router.post('/add', async (req,res)=>{
    const drugs = await loadDrugsCollection();
    await drugs.insertOne({
        name: req.body.name,
        pharmacyId: req.body.pharmacyId,
        price: req.body.price,
        unit: req.body.unit,
    })

    res.status(201).send({ msg : "Drug added successfully"});
})

router.put('/update/:id', async (req, res) => {
    const drugs = await loadDrugsCollection();
    await drugs.findOneAndUpdate({ _id: new mongodb.ObjectID(req.params.id) }, {
        $set: req.body
    })

    res.status(201).send({ msg: "Drug Updated Successfully"})
})
//other functions
// -> Import Excel File to MongoDB database

async function importExcelData2MongoDB(filePath, pharmId) {
    // -> Read Excel File to Json Data
    const excelData = excelToJson({
        sourceFile: filePath,
        sheets: [{
            // Excel Sheet Name
            name: 'Drugs',

            // Header Row -> be skipped and will not be present at our result object.
            header: {
                rows: 1
            },

            // Mapping columns to keys
            columnToKey: {
                A: 'name',
                B: 'price',
                C: 'unit'
            }
        }]
    });

    //attach pharmID
    excelData.Drugs.forEach(drug => {
        drug.pharmacyId = pharmId
    })

    const drugs = await loadDrugsCollection();
    await drugs.insertMany(excelData.Drugs, (err, res) => {
        if (err) throw err;
        console.log("Number of documents inserted: " + res.insertedCount);

    })

    fs.unlinkSync(filePath);
}




async function loadDrugsCollection() {
    const client = await mongodb.MongoClient.connect(process.env.URL, { useUnifiedTopology: true });
    return client.db(process.env.DBNAME).collection('drugs')
}

module.exports = router;
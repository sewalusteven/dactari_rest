const express = require('express');
const mongodb = require('mongodb');
const router = express.Router();

router.get('/', async (req, res) => {
    const specialities = await loadSpecialitiesCollection();
    res.send(await specialities.find({}).toArray());
})

router.post('/add', async (req, res) => {
    const specialities = await loadSpecialitiesCollection();
    await specialities.insertOne({
        speciality: req.body.speciality,
        description: req.body.description
    });

    res.status(201).send({ msg: "Speciality added successfully" });
})

async function loadSpecialitiesCollection() {
    const client = await mongodb.MongoClient.connect(process.env.URL, { useUnifiedTopology: true });
    return client.db(process.env.DBNAME).collection('specialities')
}

module.exports = router


const express = require('express');
const mongodb = require('mongodb');
const router = express.Router();
const helper = require('../../../helpers/misc')

// Connection URL
const url = 'mongodb://mongo:27017';

// Database Name
const dbName = 'dactari';

//insert request
router.post('/add', async (req, res) => {
    const docRequests = await loadDoctorRequestCollection();
    docRequests.insertOne({
        doctorId: null, //upon adding a request, a doctor has not been assigned yet by the admin
        phoneNumber: req.body.phoneNumber,
        consultationType: req.body.consultationType,
        consultationFees: req.body.consultationFees,
        appointmentDescription: req.body.appointmentDescription,
        appointmentTime: req.body.appointmentTime,
        appointmentDate: req.body.appointmentDate,
        symptom: req.body.symptom,
        patientId: new mongodb.ObjectID(req.body.patientId),
        doctorName: null,
        paymentStatus: false,
        status: "Pending",
        paymentMethod: req.body.paymentMethod
    }).then(results => {
        res.status(201).send(results)
    }).catch(err => {
        console.log(err)
    })
});

//get requests
router.post('/', async (req, res) => {
    const docRequests = await loadDoctorRequestCollection();
    const current_page = req.body.page || 1;
    const per_page = req.body.limit || process.env.PER_PAGE;

    res.status(201).send(helper.paginator(await docRequests.find({}).toArray(),current_page, per_page));
})

router.get('/single/:id', async (req, res) => {
    const docRequests = await loadDoctorRequestCollection();
    res.send(await docRequests.find({_id: new mongodb.ObjectID(req.params.id)}).toArray());
})

//update requests
router.put('/update/:id', async (req, res) => {
    const docRequests = await loadDoctorRequestCollection();
    await docRequests.findOneAndUpdate({ _id: new mongodb.ObjectID(req.params.id) }, {
        $set: req.body
    })

    res.status(201).send({ msg: "Request Updated Successfully"})
})


async function loadDoctorRequestCollection() {
    const client = await  mongodb.MongoClient.connect(url, { useUnifiedTopology: true })

    return client.db(dbName).collection('doctor_requests');

}

module.exports = router;
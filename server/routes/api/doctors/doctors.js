const express = require('express');
const mongodb = require('mongodb');
const bcrypt = require('bcrypt');
const router = express.Router()
const helper = require('../../../helpers/misc')

// Connection URL
const url = 'mongodb://mongo:27017';

// Database Name
const dbName = 'dactari';


//Get Doctors
router.post('/', async (req, res) => {
    const doctors = await loadDoctorsCollection();
    const current_page = req.body.page || 1;
    const per_page = req.body.limit || process.env.PER_PAGE;


    res.status(201).send(helper.paginator(await doctors.find({ isActive: true }).toArray(), current_page, per_page));

})

router.get('/single/:id', async (req, res) => {
    const doctors = await loadDoctorsCollection();
    res.send(await doctors.find({ _id: new mongodb.ObjectID(req.params.id) }).toArray());

})


//Add Doctors
router.post('/add', async (req, res) => {
    const doctors = await loadDoctorsCollection();


    bcrypt.genSalt(10, function (err, salt) {
        if (err) throw err;
        bcrypt.hash('P@ssw0rd123', salt, function (err, hash) {
            if (err) throw err;
            // Store hash in your password DB.
            doctors.insertOne({
                fullName: req.body.fullName,
                email: req.body.email,
                phoneNumber: req.body.phoneNumber,
                speciality: req.body.speciality,
                picture: null,
                address: {
                    longitude: null,
                    latitude: null,
                    city: req.body.city,
                    street: req.body.street
                },
                password: hash, //generating a temp password
                isActive: true,
                createdAt: new Date()
            }).then(results => {

                res.status(201).send({ msg : "Doctor added successfully"});

            }).catch(err => {
                console.log(err)
            })
        });
    });




})

//Update Doctors
router.put('/update/:id', async (req, res) => {
    const doctors = await loadDoctorsCollection();
    await doctors.findOneAndUpdate({ _id: new mongodb.ObjectID(req.params.id) }, {
        $set: req.body
    })

    res.status(201).send({ msg: "Doctor Updated Successfully" })
})

router.put('/password/:id', async (req, res) => {
    const doctors = await loadDoctorsCollection();

    bcrypt.genSalt(10, function (err, salt) {
        if (err) throw err;
        bcrypt.hash(req.body.password, salt, function (err, hash) {
            if (err) throw err;
            doctors.findOneAndUpdate({ _id: new mongodb.ObjectID(req.params.id) }, {
                $set: {
                    password: hash
                }
            }).then(results => {

                res.status(201).send({ msg: "Password Updated Successfully"})
            }).catch(err => {
                console.log(err)
            })
        })
    })




})


//connection
async function loadDoctorsCollection() {
    const client = await mongodb.MongoClient.connect(url, { useUnifiedTopology: true });

    return client.db(dbName).collection('doctors')
}


module.exports = router
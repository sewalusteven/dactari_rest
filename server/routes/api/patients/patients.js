const express = require('express');
const mongodb = require('mongodb');
const bcrypt = require('bcrypt');
const router = express.Router();
const helper = require('../../../helpers/misc')

// Connection URL
const url = 'mongodb://mongo:27017';

// Database Name
const dbName = 'dactari';


//Get Patients
router.post('/', async (req, res) => {
    const patients = await loadPatientsCollection();
    const current_page = req.body.page || 1;
    const per_page = req.body.limit || process.env.PER_PAGE;

    const results = await patients.find({}).toArray();
    const arr = [];

    results.forEach(result => {
        arr.push({
            _id: result._id,
            firstName: result.firstName,
            lastName: result.lastName,
            email: result.email,
            gender: result.gender,
            phoneNumber: result.phoneNumber,
            dob: result.dob,
            picture: result.picture,
            address: {
                city: result.adress.city,
                street: result.address.street
            },
            createdAt: result.createdAt
        })
    })

    res.status(201).send(helper.paginator(arr, current_page, per_page));
})

router.post('/login', async (req, res) => {
    const patients = await loadPatientsCollection();
    //check if email and password are filled
    if (!req.body.phoneNumber || !req.body.password) return res.status(401).json({ msg: "Missing Email and Password!" });

    const possibleUsers = await patients.find({ phoneNumber: req.body.phoneNumber }).toArray();
    if (possibleUsers.length === 0) return res.status(404).json({ msg: "User doesn't exist !!" });

    possibleUsers.forEach(patient => {
        bcrypt.compare(req.body.password, patient.password, function (err, result) {
            if (err) return res.status(404).json({ msg: "Invalid Password !!" });
            if (result == true) return res.status(201).send({
                _id: patient._id,
                firstName: patient.firstName,
                lastName: patient.lastName,
                email: patient.email,
                gender: patient.gender,
                phoneNumber: patient.phoneNumber,
                dob: patient.dob,
                picture: patient.picture,
                address: {
                    city: patient.address.city,
                    street: patient.address.street
                },
                createdAt: patient.createdAt
            });
        });
    })
})

router.get('/single/:id', async (req, res) => {
    const patients = await loadPatientsCollection();
    const results = await patients.find({ _id: new mongodb.ObjectID(req.params.id) }).toArray();
    const arr = [];

    results.forEach(result => {
        arr.push({
            _id: result._id,
            firstName: result.firstName,
            lastName: result.lastName,
            email: result.email,
            gender: result.gender,
            phoneNumber: result.phoneNumber,
            dob: result.dob,
            picture: result.picture,
            address: {
                city: result.address.city,
                street: result.address.street
            },
            createdAt: result.createdAt
        })
    })

    res.send(arr);

})


//Add Patients
router.post('/add', async (req, res) => {
    const patients = await loadPatientsCollection();

    bcrypt.genSalt(10, function (err, salt) {
        if (err) throw err;
        bcrypt.hash(req.body.password, salt, function (err, hash) {
            if (err) throw err;
            // Store hash in your password DB.
            patients.insertOne({
                firstName: req.body.firstName,
                lastName: req.body.lastName,
                email: req.body.email,
                gender: req.body.gender,
                phoneNumber: req.body.phoneNumber,
                dob: req.body.dob,
                picture: null,
                address: {
                    longitude: null,
                    latitude: null,
                    city: null,
                    street: null
                },
                password: hash,
                createdAt: new Date(),
                otpToken: '1234'
            }).then(results => {
                res.status(201).send({ msg: "Patient added Successfully" });
            }).catch(err => {
                console.log(err)
                res.status(404).send({ msg: "Something went wrong!!" });
            })
        });
    });

})

//Update Patients
router.put('/update/:id', async (req, res) => {
    const patients = await loadPatientsCollection();
    await patients.findOneAndUpdate({ _id: new mongodb.ObjectID(req.params.id) }, {
        $set: req.body
    })

    res.status(201).send({ msg: "Patient Updated Successfully" })
})

router.post('/confirm', async (req, res) => {
    const patients = await loadPatientsCollection();
    const confirm = await patients.find({ phoneNumber: req.body.phoneNumber, otpToken: req.body.otpToken }).toArray();

    if (confirm.length > 0)
        res.status(201).send({ msg: "OTP Token Validated" })
    else
        res.status(404).send({ msg: "Invalid OTP Token" })

})

router.put('/password/:id', async (req, res) => {
    const patients = await loadPatientsCollection();

    bcrypt.genSalt(process.env.saltRounds, function (err, salt) {
        if (err) throw err;
        bcrypt.hash(req.body.password, salt, function (err, hash) {
            if (err) throw err;
            // Store hash in your password DB.
            patients.findOneAndUpdate({ _id: new mongodb.ObjectID(req.params.id) }, {
                $set: {
                    password: hash
                }
            }).then(results => {
                res.status(201).send({ msg: "Password Updated Successfully" })
            }).catch(err => {
                console.log(err)
            })
        });
    });

})


//Delete Patients


//connection
async function loadPatientsCollection() {
    const client = await mongodb.MongoClient.connect(url, { useUnifiedTopology: true });

    return client.db(dbName).collection('patients')
}


module.exports = router
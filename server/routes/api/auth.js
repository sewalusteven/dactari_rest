const express = require('express');
const mongodb = require('mongodb');
const bcrypt = require('bcrypt');
const router = express.Router();


router.post('/', async (req, res) => {
    const users = await loadUsersCollection();

    //check if email and password are filled
    if(!req.body.email || !req.body.password) return res.status(401).json({ message: "Missing Email and Password!"});

    const possibleUsers = await users.find({ email: req.body.email }).toArray();
    if(possibleUsers.length === 0)return  res.status(404).json({ message: "Invalid Email and Password!" });

    possibleUsers.forEach(user => {
        bcrypt.compare(req.body.password, user.password, function(err, result) {
            if(err) throw err;
            if(result == true)return res.status(201).send(user);
        });
    })


})


//connection
async function loadUsersCollection() {
    const client = await mongodb.MongoClient.connect(process.env.URL, { useUnifiedTopology: true });

    return client.db(process.env.DBNAME).collection('users')
}


module.exports = router